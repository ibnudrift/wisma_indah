<?php
error_reporting(E_ALL ^ E_NOTICE);
require_once __DIR__.'/../vendor/autoload.php';
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();

/* Global constants */
define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT']);
define('APP_PATH', dirname(ROOT_PATH).DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR);
define('ASSETS_PATH', ROOT_PATH.DIRECTORY_SEPARATOR);

// Register Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

// Register Swiftmailer
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

// Register URL Generator
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// Register Validator
$app->register(new Silex\Provider\ValidatorServiceProvider());

// ------------------ Homepage ------------------------
$app->get('/', function () use ($app) {
	return $app['twig']->render('page/home.twig', array(
        'layout' => 'layouts/column1.twig',
        'msg' => isset($_GET['msg']) ? $_GET['msg']: '',
    ));
})
->bind('homepage');

// ------------------ about ------------------
$app->get('/about', function () use ($app) {
	return $app['twig']->render('page/about.twig', array(
        'layout' => 'layouts/inside.twig',
    ));
})
->bind('about');

// ------------------ services ------------------
$app->get('/services', function () use ($app) {
    return $app['twig']->render('page/services.twig', array(
        'layout' => 'layouts/inside.twig',
    ));
})
->bind('services');

// read brand clie
// $maindir1=  __DIR__."/images/product-brand/clie/";
// $clie = '';
// $mydir1 = opendir($maindir1);
// while($fn1 = readdir($mydir1)){
//     if (substr($fn1,-3) == 'jpg' || substr($fn1,-4) == 'jpeg' || substr($fn1,-3) == 'gif' || substr($fn1,-3) == 'png'){
//             $clie .=  $fn1."\n";
//             }
// }
// closedir($mydir1);
// $clie = array_filter(explode("\n", $clie));

// read brand ardenza
$maindir=  __DIR__."/images/product-brand/ardenza/";
$ardenza = '';
$mydir = opendir($maindir);
while($fn = readdir($mydir)){
    if (substr($fn,-3) == 'jpg' || substr($fn,-4) == 'jpeg' || substr($fn,-3) == 'gif' || substr($fn,-3) == 'png'){
            $ardenza .=  $fn."\n";
            }
}
closedir($mydir);
$ardenza = array_filter(explode("\n", $ardenza));

// read brand merzeille
$maindir2=  __DIR__."/images/product-brand/merzeille/";
$merzeille = '';
$mydir2 = opendir($maindir2);
while($fn2 = readdir($mydir2)){
    if (substr($fn2,-3) == 'jpg' || substr($fn2,-4) == 'jpeg' || substr($fn2,-3) == 'gif' || substr($fn2,-3) == 'png'){
            $merzeille .=  $fn2."\n";
            }
}
closedir($mydir2);
$merzeille = array_filter(explode("\n", $merzeille));

// read brand lightwood
$maindir3=  __DIR__."/images/product-brand/lightwood/";
$lightwood = '';
$mydir3 = opendir($maindir3);
while($fn3 = readdir($mydir3)){
    if (substr($fn3,-3) == 'jpg' || substr($fn3,-4) == 'jpeg' || substr($fn3,-3) == 'gif' || substr($fn3,-3) == 'png'){
            $lightwood .=  $fn3."\n";
            }
}
closedir($mydir3);
$lightwood = array_filter(explode("\n", $lightwood));

// read brand htl
$maindir4=  __DIR__."/images/product-brand/HTL/";
$htl = '';
$mydir4 = opendir($maindir4);
while($fn4 = readdir($mydir4)){
    if (substr($fn4,-3) == 'jpg' || substr($fn4,-4) == 'jpeg' || substr($fn4,-3) == 'gif' || substr($fn4,-3) == 'png'){
            $htl .=  $fn4."\n";
            }
}
closedir($mydir4);
$htl = array_filter(explode("\n", $htl));

$app['product_brand'] = array(
            // 'clie' => $clie,
            'ardenza' => $ardenza,
            'merzeille' => $merzeille,
            'lightwood' => $lightwood,
            'htl' => $htl,
    );

// ------------------ product/brand ------------------
$app->get('/product/brand', function () use ($app) {
    return $app['twig']->render('page/product_brand.twig', array(
        'layout' => 'layouts/inside.twig',
        'data' => $app['product_brand'],
    ));
})
->bind('product/brand');

// read brand sofa
$maindir5=  __DIR__."/images/category/sofa/";
$sofa = '';
$mydir5 = opendir($maindir5);
while($fn4 = readdir($mydir5)){
    if (substr($fn4,-3) == 'jpg' || substr($fn4,-4) == 'jpeg' || substr($fn4,-3) == 'gif' || substr($fn4,-3) == 'png'){
            $sofa .=  $fn4."\n";
            }
}
closedir($mydir5);
$sofa = array_filter(explode("\n", $sofa));

// read brand dining
$maindir_dining=  __DIR__."/images/category/dining sets/";
$dining = '';
$mydir_dining = opendir($maindir_dining);
while($fn4 = readdir($mydir_dining)){
    if (substr($fn4,-3) == 'jpg' || substr($fn4,-4) == 'jpeg' || substr($fn4,-3) == 'gif' || substr($fn4,-3) == 'png'){
            $dining .=  $fn4."\n";
            }
}
closedir($mydir_dining);
$dining = array_filter(explode("\n", $dining));

// read brand bedroom
$maindir_bedroom=  __DIR__."/images/category/bedroom sets/";
$bedroom = '';
$mydir_bedroom = opendir($maindir_bedroom);
while($fn_benroom = readdir($mydir_bedroom)){
    if (substr($fn_benroom,-3) == 'jpg' || substr($fn_benroom,-4) == 'jpeg' || substr($fn_benroom,-3) == 'gif' || substr($fn_benroom,-3) == 'png'){
            $bedroom .=  $fn_benroom."\n";
            }
}
closedir($mydir_bedroom);
$bedroom = array_filter(explode("\n", $bedroom));

// read brand coffee table
$maindir_coffe=  __DIR__."/images/category/coffee table/";
$coffee = '';
$mydir_coffe = opendir($maindir_coffe);
while($fn_coffee = readdir($mydir_coffe)){
    if (substr($fn_coffee,-3) == 'jpg' || substr($fn_coffee,-4) == 'jpeg' || substr($fn_coffee,-3) == 'gif' || substr($fn_coffee,-3) == 'png'){
            $coffee .=  $fn_coffee."\n";
            }
}
closedir($mydir_coffe);
$coffee = array_filter(explode("\n", $coffee));

// read brand wardrobes
$maindir_wardrobe=  __DIR__."/images/category/wardrobes/";
$wardrobe = '';
$mydir_wardrobe = opendir($maindir_wardrobe);
while($fn_wardrobe = readdir($mydir_wardrobe)){
    if (substr($fn_wardrobe,-3) == 'jpg' || substr($fn_wardrobe,-4) == 'jpeg' || substr($fn_wardrobe,-3) == 'gif' || substr($fn_wardrobe,-3) == 'png'){
            $wardrobe .=  $fn_wardrobe."\n";
            }
}
closedir($mydir_wardrobe);
$wardrobe = array_filter(explode("\n", $wardrobe));

// read brand asesoris
$maindir_accessories=  __DIR__."/images/category/accessories/";
$accessories = '';
$mydir_accessories = opendir($maindir_accessories);
while($fn_accessories = readdir($mydir_accessories)){
    if (substr($fn_accessories,-3) == 'jpg' || substr($fn_accessories,-4) == 'jpeg' || substr($fn_accessories,-3) == 'gif' || substr($fn_accessories,-3) == 'png'){
            $accessories .=  $fn_accessories."\n";
            }
}
closedir($mydir_accessories);
$accessories = array_filter(explode("\n", $accessories));

$app['product_category'] = array(
            'sofa' => $sofa,
            'dining' => $dining,
            'bedroom' => $bedroom,
            'coffee' => $coffee,
            'wardrobes' => $wardrobe,
            'asesoris' => $accessories,
    );

// ------------------ product/category ------------------
$app->get('/product/category', function () use ($app) {
    return $app['twig']->render('page/product_category.twig', array(
        'layout' => 'layouts/inside.twig',
        'data'=>$app['product_category'],
    ));
})
->bind('product/category');

// ------------------ Contact Us ------------------
$app->match('/contact-us', function (Request $request) use ($app) {
    
    // $data = $request->get('Contact');
    // if ($data == null) {
    //     $data = array(
    //         'name' => '',
    //         'email' => '',
    //         'phone' => '',
    //         'address' => '',
    //         'city' => '',
    //         'message'=>'',
    //     );
    // }

    // if ($_POST) {

    //     $constraint = new Assert\Collection(array(
    //         'name' => new Assert\NotBlank(),
    //         'email' => array(new Assert\Email(), new Assert\NotBlank()),
    //         'phone' => new Assert\Length(array('max'=>2000)),
    //         'address' => new Assert\NotBlank(),
    //         'city' => new Assert\Length(array('max'=>2000)),
    //         'message' => new Assert\Length(array('max'=>2000)),
    //     ));

    //     $errors = $app['validator']->validateValue($data, $constraint);

    //     $errorMessage = array();
    //     if (count($errors) > 0) {
    //         foreach ($errors as $error) {
    //             $errorMessage[] = $error->getPropertyPath().' '.$error->getMessage();
    //         }
    //     } else {
    //         $pesan = \Swift_Message::newInstance()
    //             ->setSubject('Hi, Contact Website Dauni')
    //             ->setFrom(array('no-reply@daunilifestyle.com'))
    //             ->setTo(array('deoryzpandu@gmail.com', 'ibnudrift@gmail.com', 'info@daunilifestyle.com', $data['email']))
    //             ->setReplyTo(array('info@daunilifestyle.com'))
    //             ->setBody($app['twig']->render('page/mail.twig', array(
    //                 'data' => $data,
    //             )), 'text/html');

    //         $app['mailer']->send($pesan);
    //         return $app->redirect($app['url_generator']->generate('contactus').'?msg=success');
    //     }
    // }

    return $app['twig']->render('page/contactus.twig', array(
        'layout' => 'layouts/inside.twig',
        'error' => ($errorMessage)? $errorMessage : null,
        'data' => ($data)? $data : null,
        'msg' => ($_GET['msg'])? $_GET['msg'] : null,
    ));
})
->bind('contactus');

// ------------------ reserve ------------------
$app->post('/reserve', function (Request $request) use ($app) {
    
    $data = $request->get('reservation');
    if ($data == null) {
        $data = array(
            'name' => '',
            'email' => '',
            'phone' => '',
            'no_pax' => '',
            'date' => '',
            'time' => '',
        );
    }

   $pesan = \Swift_Message::newInstance()
                ->setSubject('Hi, Reserve Website Dauni')
                ->setFrom(array('no-reply@daunilifestyle.com'))
                ->setTo(array('deoryzpandu@gmail.com', 'ibnudrift@gmail.com', 'info@daunilifestyle.com', $data['email']))
                ->setReplyTo(array('info@daunilifestyle.com'))
                ->setBody($app['twig']->render('page/reserve_mail.twig', array(
                    'data' => $data,
                )), 'text/html');

    $app['mailer']->send($pesan);

    return $app->redirect($app['url_generator']->generate('homepage').'?msg=success_reseve');
})
->bind('reserve');

// ------------------ SUBSCRIBE ------------------
$app->post('/subscribe', function (Request $request) use ($app) {
    
    $data = $request->get('contact');
    if ($data == null) {
        $data = array(
            'email' => '',
        );
    }

    $pesan = \Swift_Message::newInstance()
        ->setSubject('Request for consultation')
        ->setFrom(array('no-reply@btscommodity.com'))
        ->setTo(array('btscom@rad.net.id','deoryzpandu@gmail.com', $data['email'], 'info@btscommodity.com'))
        ->setReplyTo(array('btscom@rad.net.id'))
        ->setBody($app['twig']->render('page/subscribe.twig', array(
            'data' => $data,
        )), 'text/html');

    $app['mailer']->send($pesan);
    return $app->redirect($app['url_generator']->generate('homepage').'?msg=success');
})
->bind('subscribe');



$app['debug'] = true;

$app->run();